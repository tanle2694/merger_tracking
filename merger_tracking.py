import os
import shutil
import numpy as np
import time
from datetime import datetime
from embs import init
import cv2
import tensorflow as tf
import shutil
import networkx as nx

def compute_euclidean_distance(x, y):
    """
    Computes the euclidean distance between two tensorflow variables
    """
    x = tf.expand_dims(x, 1)
    with tf.name_scope('euclidean_distance') as scope:
        d = tf.sqrt(tf.reduce_sum(tf.square(x - y), 2))
        return d


query_t = tf.placeholder(tf.float32, (None, None))
test_t = tf.placeholder(tf.float32, (None, None))

tensor_1 = compute_euclidean_distance(query_t, test_t)


def split_timeseries(timeseries):
    timeseries = np.array(timeseries)
    different = np.ediff1d(timeseries)
    where = np.where(different > 10)[0]
    return where, different[where]


def compare_2_list(embs1, embs2, times, sess):

    time1_start, time1_stop, time2_start, time2_stop = times
    if ((time2_start < time1_stop) and (time2_start > time1_start)) or (
        (time2_stop < time1_stop) and (time2_stop > time1_start)):
        return False, None

    if int(time2_start) - int(time1_stop) > 5000:
        return False, None
    embs1 = np.array(embs1)
    embs2 = np.array(embs2)

    dis_matrix = sess.run(tensor_1, feed_dict={query_t: embs1, test_t: embs2})

    min_dis = np.min(dis_matrix)
    if np.any(dis_matrix < 10):
        satisfy = np.unravel_index(dis_matrix.argmin(), dis_matrix.shape)
        return True, [satisfy, min_dis]
    else:
        return False, min_dis


def process_tracking_split(files):
    times = []
    for file in files:
        times.append(int(file.split("/")[-1].split("_")[0])/ 1000)
    times, files = zip(*sorted(zip(times, files)))
    times = np.array(times)
    split = split_timeseries(times)
    files = np.split(files, split[0] + 1)

    return files

def copy_list_image(list_image, folder_save):
    f = open(os.path.join(folder_save, "check.txt"), "a")
    for i in list_image:
        shutil.copy(i, os.path.join(folder_save, i.split("/")[-1]))
        f.write(i + "\n")
    f.write("------------------------------- \n")
    f.close()


def check_within_a_timeseries(timeseries_links, sess, input_tensor, output_tensor):
    dict_data = {}
    for link in timeseries_links:
        tracking_id = link.split("/")[-1].split("_")[1]
        if not dict_data.has_key(tracking_id):
            dict_data[tracking_id] = {}
            dict_data[tracking_id]["link"] = []
        dict_data[tracking_id]["link"].append(link)

    for tracking_id in dict_data.keys():
        links = dict_data[tracking_id]["link"]
        links = process_tracking_split(links)
        if len(links) > 1:
            for k in range(len(links)):
                dict_data[tracking_id + "_" + str(k)] = {}
                dict_data[tracking_id+ "_" + str(k)]["link"] = links[k]
            dict_data.pop(tracking_id, None)
        else:
            dict_data[tracking_id]["link"] = links[0]

    for tracking_id in dict_data.keys():
        dict_data[tracking_id]["embs"] = []
        dict_data[tracking_id]["times"] = []
        for link in list(dict_data[tracking_id]["link"]):

            img = cv2.imread(link)
            img = cv2.resize(img, (224, 224))
            emb = sess.run(output_tensor, feed_dict={input_tensor: [img]})
            dict_data[tracking_id]["embs"].append(emb[0])

    start = []
    for key in dict_data.keys():
        links = dict_data[key]["link"]
        times = [(link.split("/")[-1].split("_")[0]) for link in links]
        times.sort()
        time_start = times[0]
        time_stop = times[-1]
        dict_data[key]["time_start"] = time_start
        dict_data[key]["time_stop"] = time_stop
        start.append(time_start)

    tracking_id = dict_data.keys()
    start , tracking_id =zip(*sorted(zip(start, tracking_id)))

    merger = []
    S = []
    tick = np.full(len(tracking_id), -1)

    for i in range(len(tracking_id)):
        group = [i]
        if tick[i] != -1:
            continue

        tick[i] = i
        j = i
        start = i
        result = []
        while True:
            j += 1
            if j == len(tracking_id):
                break
            if tick[j] != -1:
                continue
            times = [dict_data[tracking_id[start]]["time_start"], dict_data[tracking_id[start]]["time_stop"],
                     dict_data[tracking_id[j]]["time_start"], dict_data[tracking_id[j]]["time_stop"]]
            check, satisfy = compare_2_list(dict_data[tracking_id[start]]["embs"], dict_data[tracking_id[j]]["embs"], times, sess)

            if check:
                print(satisfy)
                link1 = dict_data[tracking_id[start]]["link"][satisfy[0][0]]
                link2 = dict_data[tracking_id[j]]["link"][satisfy[0][1]]
                dis = satisfy[1]
                result.append([link1, link2 , dis])

                group.append(j)
                start = j
                tick[j] = i
        S.append(result)
        merger.append(group)

    print(len(tracking_id))
    print("len merger", len(merger))
    print("len S", len(S))
    for (s, group) in enumerate(merger):
        tk_id = []
        for i in group:
            tk_id.append(tracking_id[i])
        name_folder_save = "__".join(tk_id)
        name_folder_save = "/data/result/" + name_folder_save
        try:
            os.mkdir(name_folder_save)
        except:
            t = 0
            while True:
                if os.path.exists(name_folder_save + "ver" + str(t)):
                    print("continue")
                    t += 1
                else:
                    try:
                        os.mkdir(name_folder_save + "ver" + str(t))
                    except:
                        name_folder_save = "/data/result/" + str(time.time())
                        os.mkdir(name_folder_save + "ver" + str(t))
                    name_folder_save = name_folder_save + "ver" + str(t)

                    break

        for tk in tk_id:
            # print(dict_data[tk]["link"])
            copy_list_image(dict_data[tk]["link"], name_folder_save)
        f = open(os.path.join(name_folder_save, "debug.txt"), "w")
        for dis in S[s]:
            print(dis)
            f.write("+++++++++++ \n")
            f.write(dis[0] + "\n")
            f.write((dis[1] + "\n"))
            f.write(str(dis[2])+  "\n")
        f.close()





    print("END check_within_a_timeseries")


def process_folder_image(path):
    """

    :param path: String
    :return: file split with time series
    """
    time_series = []
    file_image = []
    for file in os.listdir(path):
        file_split = file.split("_")
        time_series.append(int(file_split[0]) / 1000)
        file_image.append(os.path.join(path, file))

    time_series, file_image = zip(*sorted(zip(time_series, file_image)))
    file_image = np.array(file_image)
    b = split_timeseries(time_series)
    file_split = np.split(file_image, b[0] + 1)
    return file_split





sess, input_tensor, output_tensor = init()
path = "/data/data/kidsplaza/18_07/00021/"


file_split = process_folder_image(path)

if os.path.exists("/data/result"):
    shutil.rmtree("/data/result")

try:
    os.mkdir("/data/result")
except:
    pass

for i in range(len(file_split)):
    check_within_a_timeseries(file_split[i], sess, input_tensor, output_tensor)

