from importlib import import_module
import tensorflow as tf
import cv2
import numpy as np
import shutil
import time
import os
import pickle






def compute_euclidean_distance(x, y):
    """
    Computes the euclidean distance between two tensorflow variables
    """
    x = tf.expand_dims(x, 1)
    with tf.name_scope('euclidean_distance') as scope:
        # d = tf.square(tf.sub(x, y))
        # d = tf.sqrt(tf.reduce_sum(d)) # What about the axis ???
        d = tf.sqrt(tf.reduce_sum(tf.square(x - y), 2))
        return d
def init():
    images = tf.placeholder(tf.float32, [None, 224, 224, 3])
    model = import_module('nets.' + "resnet_v1_50")
    head = import_module('heads.' + "fc1024")
    endpoints, body_prefix = model.endpoints(images, is_training=False)
    with tf.name_scope('head'):
        endpoints = head.head(endpoints, 128, is_training=False)
    checkpoint = "weights/checkpoint-25000"

    config = tf.ConfigProto()
    config.gpu_options.allow_growth = True

    sess = tf.InteractiveSession(config=config)

    tf.train.Saver().restore(sess, checkpoint)
    return sess, images, endpoints["emb"]

